<?php

/**
 * Implements hook_rules_event_info
 *
 * Declares CPWeb Rules events
 *
 * @return array
 */
function cpwidget_rules_event_info() {
  $items = array(
    'cpwidget_callback' => array(
      'label' => t('Callback received'),
      'group' => t('CPWidget'),
      'variables' => array(
        'txtype' => array(
          'type' => 'text',
          'label' => t('TxType'),
        ),
        'status' => array(
          'type' => 'text',
          'label' => t('Status'),
        ),
        'values' => array(
          'type' => 'entity',
          'label' => t('Values'),
        ),
        'validated' => array(
          'type' => 'integer',
          'label' => t('Validated')
        ),
      ),
    ),

    'cpwidget_return' => array(
      'label' => t('Page Return'),
      'group' => t('CPWidget'),
      'variables' => array(
        'values' => array(
          'type' => 'entity',
          'label' => t('Values'),
        ),
      ),
    ),

    'cpwidget_callback_validation' => array(
        'label' => t('Callback Validation'),
        'group' => t('CPWidget'),
        'variables' => array(
          'values' => array(
            'type' => 'entity',
            'label' => t('Values'),
          ),
          'validated' => array(
            'type' => 'integer',
            'label' => t('Validated')
          ),
        ),
    ),
  );

  return $items;
}