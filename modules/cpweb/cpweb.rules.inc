<?php

/**
 * Implements hook_rules_event_info
 *
 * Declares CPWeb Rules events
 *
 * @return array
 */
function cpweb_rules_event_info() {
  $items = array(
    'cpweb_callback' => array(
      'label' => t('Callback received'),
      'group' => t('CPWeb'),
      'variables' => array(
        'txtype' => array(
          'type' => 'text',
          'label' => t('TxType'),
        ),
        'status' => array(
          'type' => 'text',
          'label' => t('Status'),
        ),
        'values' => array(
          'type' => 'entity',
          'label' => t('Values'),
        ),
      ),
    ),

    'cpweb_return' => array(
      'label' => t('Page Return'),
      'group' => t('CPWeb'),
      'variables' => array(
        'status' => array(
            'type' => 'text',
            'label' => t('Status'),
        ),
        'values' => array(
            'type' => 'entity',
            'label' => t('Values'),
        ),
      ),
    ),
  );

  return $items;
}