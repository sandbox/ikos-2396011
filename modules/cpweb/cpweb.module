<?php

/**
 * @file
 * Provides an example payment method for Drupal Commerce for testing and
 *   development.
 */

/**
 * Implements hook_form_alter
 *
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function cpweb_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'rsm2000_settings_form') {
    $form['logging']['cpweb_log_callbacks'] = array(
      '#type' => 'checkbox',
      '#title' => t('CPWeb'),
      '#default_value' => variable_get('cpweb_log_callbacks', FALSE),
    );
  }
}

/**
 * Implements hook_menu
 *
 * @return array
 */
function cpweb_menu() {
  $items = array();

  // Success callback.
  $items['cpweb/callback'] = array(
      'page callback' => 'cpweb_callback',
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
  );

  // return page
  $items['cpweb/return'] = array(
      'page callback' => 'cpweb_return_page',
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Callback - Success or Fail
 *
 * This callback is always called when configured at the Campaign,
 * and contains all parameters.
 */
function cpweb_callback() {
  // grab POST
  $result = $_POST;

  // log if need be
  if (variable_get('cpweb_log_callbacks', FALSE)) {
    watchdog('cpweb', 'Success Callback : <pre>' . print_r($result, TRUE) . '</pre>');
  }

  // lookup transaction
  $transaction_id = $result['M_transaction_id'];
  $transaction = commerce_payment_transaction_load($transaction_id);

  // lookup order
  $order_id = isset($result['M_order_id']) ? $result['M_order_id'] : FALSE;
  $order = empty($order_id) ? FALSE : commerce_order_load($order_id);

  // check result and set transaction status accordingly
  $status = isset($result['baseStatus']) ? $result['baseStatus'] : 'INVALID';
  $txtype = isset($result['txtype']) ? $result['txtype'] : '';

  if ($txtype == 'PAYMENT') {
    if ($status == 'OK') {
      // great, transaction was accepted
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      if (!empty($order)) {
        commerce_payment_redirect_pane_next_page($order);
      }
      else {
        watchdog('cpweb', 'Error, unable to load order : <pre>' . print_r($result, TRUE) . '</pre>');
      }
    }
    else {
      // invalid/Fail
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      if (!empty($order)) {
        commerce_payment_redirect_pane_previous_page($order);
      }
      else {
        watchdog('cpweb', 'Error, unable to load order : <pre>' . print_r($result, TRUE) . '</pre>');
      }
    }
  }

  // save transaction to update
  commerce_payment_transaction_save($transaction);

  // RULES EVENT
  rules_invoke_event('cpweb_callback', $txtype, $status, (object)$result);
}

/**
 * Return Page
 *
 * This is the Page that is configured in the Campaign for the user to return to.
 * It simply redirects to Commerce standard payment return page,
 * that was passed in as a transparent parameter
 */
function cpweb_return_page() {
  // grab POST
  $result = $_POST;

  // log if need be
  if (variable_get('cpweb_log_callbacks', FALSE)) {
    watchdog('cpweb', 'Return Page : <pre>' . print_r($result, TRUE) . '</pre>');
  }

  // check status
  $status = isset($result['baseStatus']) ? $result['baseStatus'] : 'INVALID';

  // RULES EVENT
  rules_invoke_event('cpweb_return', $status, (object)$result);

  // @TODO - check return value and redirect to failed page ?

  if (isset($result['M_payment_cb'])) {
    drupal_goto($result['M_payment_cb']);
  }

  drupal_goto('checkout');
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function cpweb_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['cpweb_purchase'] = array(
    'title' => t('CPWeb Purchase'),
    'description' => t('Purchases using CPWeb off-site payments, from RSM2000'),
    'active' => TRUE,
    'terminal' => FALSE,
    'offsite' => TRUE,
    'offsite_autoredirect' => TRUE,
    'callbacks' => array(),
  );
  $payment_methods['cpweb_donate'] = array(
    'title' => t('CPWeb Donation'),
    'description' => t('Donations using CPWeb off-site payments, from RSM2000'),
    'active' => TRUE,
    'terminal' => FALSE,
    'offsite' => TRUE,
    'offsite_autoredirect' => TRUE,
    'callbacks' => array(),
  );
  
  return $payment_methods;
}

function cpweb_purchase_settings_form($settings = NULL) {
  return cpweb_settings_form($settings);  
}

function cpweb_donate_settings_form($settings = NULL) {
  return cpweb_settings_form($settings);  
}

/**
 * Payment method settings form
 *
 * @param null $settings
 * @return array
 */
function cpweb_settings_form($settings = NULL) {
  $form = array();

  $form['campaign'] = array(
    '#type' => 'fieldset',
    '#title' => t('Campaign'),

    'cpweb_campaign_id' => array(
      '#type' => 'textfield',
      '#title' => t('Campaign ID'),
      '#default_value' => isset($settings['campaign']['cpweb_campaign_id']) ?
              $settings['campaign']['cpweb_campaign_id'] : '',
      '#description' => t('The campaign id provided to you by RSM eg. 1234.567'),  
      '#required' => TRUE,  
    ),
    
    'cpweb_donor_id' => array(
      '#type' => 'textfield',
      '#title' => t('Donor Reference'),
      '#default_value' => isset($settings['campaign']['cpweb_donor_id']) ?
              $settings['campaign']['cpweb_donor_id'] : '',
      '#description' => t('Optional donor reference code'),  
    ),
    
    'cpweb_appeal_code' => array(
      '#type' => 'textfield',
      '#title' => t('Appeal Code'),
      '#default_value' => isset($settings['campaign']['cpweb_appeal_code']) ?
              $settings['campaign']['cpweb_appeal_code'] : '',
      '#description' => t('Optional appeal code'),  
    ),
   
    'cpweb_unique_id_prefix' => array(
      '#type' => 'textfield',
      '#title' => t('Unique ID prefix'),
      '#default_value' => isset($settings['cpwidget_unique_id_prefix']) ?
              $settings['cpwidget_unique_id_prefix'] : '',
      '#description' => t('Optional prefix appended to all transactions sent to RSM'),    
    ),
      
    'cpweb_mode' => array(
      '#title' => t('Payment Server'),  
      '#type' => 'radios',
      '#options' => array('demo' => 'Test', 'live' => 'Live'),
      '#default_value' => isset($settings['cpweb_mode']) ? $settings['cpweb_mode'] : 'demo',
      '#description' => t('Select whether you wish carry out transactions on the test server or live server'),  
    ),   
  );

  return $form;
}

function cpweb_purchase_redirect_form($form, &$form_state, $order, $payment_method) {
  return cpweb_redirect_form($form, $form_state, $order, $payment_method);  
}

function cpweb_donate_redirect_form($form, &$form_state, $order, $payment_method) {
  return cpweb_redirect_form($form, $form_state, $order, $payment_method);  
}

/**
 * Implements hook_redirect_form().
 */
function cpweb_redirect_form($form, &$form_state, $order, $payment_method) {

  // set the form action to the correct endpoint
  $mode = isset($payment_method['settings']['cpweb_mode']) ? $payment_method['settings']['cpweb_mode'] : 'demo';
  $form['#action'] = _rsm2000_get_endpoint('cpweb', $mode);

  // get the transaction and add payload to the form
  $transaction = cpweb_transaction($payment_method, $order);
  foreach ($transaction->payload as $name => $value) {
    if (!empty($value)) {
      $form[$name] = array('#type' => 'hidden', '#value' => $value);
    }
  }

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Proceed'),
  );

  return $form;
}

/**
 * Helper function to format all the required data for a CPWeb transaction
 *
 * @param $payment_method
 * @param $order
 * @return mixed
 */
function cpweb_transaction($payment_method, $order) {
  //watchdog('cpweb', '$payment_method : <pre>' . print_r($payment_method, TRUE) . '</pre>');

  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  // get the total of this order
  $total = $wrapper->commerce_order_total->value();
  // Load customer profile.
  $profile = commerce_customer_profile_load($order->commerce_customer_billing[LANGUAGE_NONE][0]['profile_id']);
  // Get user billing address.
  $billing_address = $profile->commerce_customer_address[LANGUAGE_NONE][0];
  // build an array of the transaction settings
  $data = cpweb_transaction_data($payment_method, $order, $billing_address);
  // construct a payment callback url for pssing through to the success page
  $payment_cb = 'checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'];
  // add it to the data
  $data['M_payment_cb'] = $payment_cb;

  // now create a commerce transaction
  $transaction = commerce_payment_transaction_new($payment_method['method_id'],$order->order_id);
  // set instance id
  $transaction->instance_id = $payment_method['instance_id'];
  // set initial status
  $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
  // add transaction amount
  $transaction->amount = $total['amount'];
  $transaction->currency_code = $total['currency_code'];
  // save to update
  commerce_payment_transaction_save($transaction);

  // add transaction id to data
  $data['M_transaction_id'] = $transaction->transaction_id;
  // and add data as payload
  $transaction->payload = $data;

  // save to update
  commerce_payment_transaction_save($transaction);

  return $transaction;
}

/**
 * Build an array of Transaction data to be sent to the CPWeb service
 *
 * @param $payment_method
 * @param $order
 * @param $profile
 * @return array
 */
function cpweb_transaction_data($payment_method, $order, $profile) {
  
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  // get the order total
  $total = $wrapper->commerce_order_total->value();
  $ext_price = commerce_currency_amount_to_decimal($total['amount'], $total['currency_code']);
  
  // get campaign settings
  $campaign = isset($payment_method['settings']['campaign']) ? $payment_method['settings']['campaign'] : FALSE;
  $prefix = isset($payment_method['settings']['cpweb_unique_id_prefix']) ? $payment_method['settings']['cpweb_unique_id_prefix']: 'web';
  
  // create a populate the return array
  // @TODO - expand this
  $data = array(
      'campaign' => isset($campaign['cpweb_campaign_id']) ? $campaign['cpweb_campaign_id'] : '',
      'donorref' => isset($campaign['cpweb_donor_id']) ? $campaign['cpweb_donor_id'] : '',
      'appealcode' => isset($campaign['cpweb_appeal_code']) ? $campaign['cpweb_appeal_code'] : '',

      'uniqueid' => _rsm2000_get_uniqueid($prefix, $order),
      'M_order_id' => $order->order_id,
      'M_user_id' => $order->uid,

      'firstname' => isset($profile['first_name']) ? $profile['first_name'] : '',
      'surname' => isset($profile['last_name']) ? $profile['last_name'] : '',
      'cardholder' => isset($profile['name_line']) ? $profile['name_line'] : '',

      'address1' => isset($profile['thoroughfare']) ? $profile['thoroughfare'] : '',
      'town' => isset($profile['locality']) ? $profile['locality'] : '',
      'postcode' => isset($profile['postal_code']) ? $profile['postal_code'] : '',
      'country' => isset($profile['country']) ? rsm2000_alpha2_to_alpha3_country_code($profile['country']) : '',
  );

  if ($payment_method['method_id'] == 'cpweb_purchase'){
    $data['purchasefix'] = $ext_price;
  }
  else if ($payment_method['method_id'] == 'cpweb_donate') {
    $data['donationfix'] = $ext_price;
    // For donation check for gift aid field on order
    if ($wrapper->__isset('field_gift_aid')){
      $gift_aid = $wrapper->field_gift_aid->value();
      $data['giftaid'] = !empty($gift_aid) ? 'Y' : 'N';
    }    
  }
  
  $additional_fields = array(
    'donorref',
    'purchasesug',
    'purchasemin',
    'purchaseminalt',
    'purchasedesc',  
    'donationsug',
    'donationmin',
    'repeat',
    'teltype',
    'tel',
    'giftaid',
    'datapromail',
    'dataproemail',
    'dataprosms',
    'dataprophone',
    'cookiesaccepted',
  );
  
  foreach ($additional_fields as $additional_field) {
    if (isset($order->data[$additional_field])){
      $data[$additional_field] = $order->data[$additional_field];
    }  
  }
  
  // allow other modules a chance to alter the transaction data
  drupal_alter('cpweb_transaction_data', $data);
  return $data;
}

/**
 * Helper function to extract an order id from a POST callback
 *
 * @param bool $data
 * @return bool
 */
function _cpweb_extract_orderid($data=FALSE) {
  // if no data was passed in then try use the POST
  if ($data === FALSE) {
    $data = $_POST;
  }
  // look for transparent parameter M_order_id
  $order_id = isset($data['M_order_id']) ? $data['M_order_id'] : FALSE;
  if (empty($order_id)) {
    // not found, so let's look for the uniqueid, as that starts with order id
    $unique_id = isset($data['uniqueid']) ? $data['uniqueid'] : FALSE;
    $parts = explode('-', $unique_id);
    if (count($parts) > 1 && is_numeric($parts[1])) {
      $order_id = $parts[1];
    }
  }
  return $order_id;
}